# [RabbitMQ Tutorials](https://www.rabbitmq.com/getstarted.html)

## 1 "Hello World!" em JavaScript

1. Primeiramente foi instalado o rabbitmq seguindo os passos de instalação do site - [Downloading and Installing RabbitMQ](https://www.rabbitmq.com/download.html)

2. Depois foi inicioado  o serviço rabbitmq pelo comando ``rabbitmq-server ``

3. Criado o arquivo send.js e tornado executável pelo comando ``chmod +x send.js``
![send.js](img/send.png)

4. Criado o arquivo receive.js e tornado executável pelo comando ``chmod +x receive.js``
![receive.js](img/receive.png)

5. Funcionamento
![Funcionamento](img/funcionamento.png)